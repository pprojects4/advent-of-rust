use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

const RED_MAX: i32 = 12;
const GREEN_MAX: i32 = 13;
const BLUE_MAX: i32 = 14;

fn main() {
    let mut total = 0;

    let file = File::open("./input.txt").expect("Unable to open file");
    let reader = BufReader::new(file);

    for raw_line in reader.lines() {
        let line = match raw_line {
            Ok(l) => l,
            Err(_) => panic!("Unable to parse line"),
        };

        let (_game_num, results_line) = match line.split_once(":") {
            None => panic!("Unable to extract game"),
            Some((g, res_line)) => match g.split_once(" ") {
                None => panic!("Unable to extract game number"),
                Some((_, v)) => {
                    let parsed_num: i32 = v.parse().expect("Unable to parse game number");
                    (parsed_num, res_line)
                }
            },
        };

        let mut valid_game = true;

        let mut color_map = HashMap::new();
        color_map.insert("red", 0);
        color_map.insert("green", 0);
        color_map.insert("blue", 0);

        let mut max_map = HashMap::new();
        max_map.insert("red", RED_MAX);
        max_map.insert("green", GREEN_MAX);
        max_map.insert("blue", BLUE_MAX);

        'game_loop: for game in results_line.split(";") {
            for raw_group in game.split(", ") {
                let group = raw_group.trim();
                let (num, color) = match group.split_once(" ") {
                    None => panic!("Unable to split cube results"),
                    Some((n, c)) => {
                        let parsed_num: i32 = n.parse().expect("Unable to parse number");
                        (parsed_num, c)
                    }
                };
                let color_var = color_map.entry(color.clone()).or_insert(0);
                let _color_max = max_map.get(color).unwrap();

                if num > *color_var {
                    *color_var = num
                }
            }
        }

        println!("{valid_game} | {line}");

        if valid_game {
            let red_count = color_map.get("red").unwrap();
            let green_count = color_map.get("green").unwrap();
            let blue_count = color_map.get("blue").unwrap();
            let power_of = red_count * green_count * blue_count;
            println!("{power_of} > {red_count} | {green_count} | {blue_count}");
            total += power_of
        }
    }

    println!("Total: {}", total);
}
