use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::ops::Add;

fn main() {
    let file = File::open("./input.txt").expect("Unable to open file");
    let reader = BufReader::new(file);
    
    let mut num_map = HashMap::new();
    num_map.insert("one", "1");
    num_map.insert("two", "2");
    num_map.insert("three", "3");
    num_map.insert("four", "4");
    num_map.insert("five", "5");
    num_map.insert("six", "6");
    num_map.insert("seven", "7");
    num_map.insert("eight", "8");
    num_map.insert("nine", "9");

    let mut totals = vec![];

    for line in reader.lines() {
        match line {
            Err(e) => println!("EEEEEEEEE {}", e),
            Ok(l) => {
                let mut nums: Vec<i8> = Vec::new();
                let mut sub_str = String::new();
                for c in l.chars() {
                   if c.is_numeric() {
                       nums.push(c.to_string().parse().expect("Unable to parse number"));
                   } else {
                      sub_str = sub_str.add(&c.to_string());
                      for (k, v) in num_map.iter() {
                          if sub_str.contains(k) {
                              nums.push(v.parse().expect("Unable to parse number"));
                              sub_str = c.to_string();
                          }
                      }
                   }
                }
                let num: i32 = format!("{}{}", nums[0], nums[nums.len() -1]).parse().expect("Unable to parse number");
                totals.push(num.clone());
            },
        }
    }

    let total: i32 = totals.iter().sum();
    println!("Total is {}", total);
}   
