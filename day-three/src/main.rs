use std::fs::File;
use std::io::{BufRead, BufReader};
use day_three::part_two::calculate as calulate_part_two;

fn main() {
    let file = File::open("./input.txt").expect("Unable to open file");
    let reader = BufReader::new(file);

    let mut parsed_lines: Vec<Vec<String>> = vec![];
    let mut line_strings: Vec<String> = vec![];

    for raw_line in reader.lines() {
        match raw_line {
            Err(_) => panic!("Unable to parse line"),
            Ok(line) => {
                line_strings.push(line.clone());
                let mut char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
                char_vec.push(".".to_string());
                parsed_lines.push(char_vec);
            }
        }
    }
    let total = calculate(parsed_lines.clone());
    println!("Part 1 Total: {total}");

    let part_two_total = calulate_part_two(line_strings);
    println!("Part 2 Total: {part_two_total}");
}

fn calculate(parsed_lines: Vec<Vec<String>>) -> i32 {
    let mut total = 0;
    let symbols = "&$@+-*/=#%";

    for line_i in 0..parsed_lines.len() {
        let line = parsed_lines[line_i].clone();
        let mut sub_str = String::new();
        'col_loop: for col_i in 0..line.len() {
            let char = line[col_i].clone();
            // println!("000000 {char}");
            if "0123456789".contains(char.as_str()) {
                sub_str = format!("{sub_str}{char}")
            } else if !sub_str.is_empty() {
                // println!("111111 {char}");
                let mut match_found;
                let num_len = sub_str.len();
                let mut start_index = col_i - num_len;
                if start_index > 0 {
                    start_index -= 1;
                }
                // check above
                if line_i != 0 {
                    for el in parsed_lines[line_i - 1][start_index..=col_i].iter() {
                        (match_found, sub_str, total) = check_char(el, symbols, total, sub_str);
                        if match_found {
                            continue 'col_loop;
                        }
                    }
                }
                // check before
                let before_el = &parsed_lines[line_i][start_index];
                (match_found, sub_str, total) = check_char(before_el, symbols, total, sub_str);

                // check after
                if col_i < line.len() {
                    let after_el = &parsed_lines[line_i][col_i];
                    (match_found, sub_str, total) = check_char(after_el, symbols, total, sub_str);
                    if match_found {
                        continue 'col_loop;
                    }
                }
                // check below
                if line_i + 1  < parsed_lines.len() {
                    for el in parsed_lines[line_i + 1][start_index..=col_i].iter() {
                        (match_found, sub_str, total) = check_char(el, symbols, total, sub_str);
                        if match_found {
                            continue 'col_loop;
                        }
                    }
                }
                sub_str.clear();
            }
        }
    }
    total
}

fn check_char(el: &String, symbols: &str, total: i32, sub_str: String) -> (bool, String, i32) {
    if "0123456789".contains(el) || el == "." {
        (false, sub_str, total)
    } else {
        let as_num: i32 = sub_str.parse().expect("Unable to parse number");
        let new_total = total + as_num;
        let new_str = String::new();
        (true, new_str, new_total)
    }
}


#[test]
fn test_same_line() {
    let raw_lines  = vec![
        String::from("*1..........-.1.%......1="),
    ];
    let mut test_lines = vec![];
    for line in raw_lines.iter() {
        let char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
        test_lines.push(char_vec);
    }
    let total = calculate(test_lines);
    println!("Total is {total}");
    assert!(total == 2);
}

#[test]
fn test_no_symbols() {
    let raw_lines  = vec![
        String::from(".................1....."),
        String::from("1...........1.......1.."),
        String::from("........1.............."),
    ];
    let mut test_lines = vec![];
    for line in raw_lines.iter() {
        let char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
        test_lines.push(char_vec);
    }
    let total = calculate(test_lines);
    println!("Total is {total}");
    assert!(total == 0);
}

#[test]
fn test_all_symbols() {
    // "&$@+-*/=#%"
    let raw_lines  = vec![
        String::from("1$......-.....@1.......+1.."),
        String::from("........1........1........."),
        String::from(".....=.........../....*...."),
        String::from(".#....1......%.......1....."),
        String::from(".1.......1..1.....1........"),
        String::from("........*..........&......."),
    ];
    let mut test_lines = vec![];
    for line in raw_lines.iter() {
        let char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
        test_lines.push(char_vec);
    }
    let total = calculate(test_lines);
    println!("Total is {total}");
    assert!(total == 11);
}

#[test]
fn test_sample_input() {
    let raw_lines = vec![
        String::from("467..114.."),
        String::from("...*......"),
        String::from("..35..633."),
        String::from("......#..."),
        String::from("617*......"),
        String::from(".....+.58."),
        String::from("..592....."),
        String::from("......755."),
        String::from("...$.*...."),
        String::from(".664.598.."),
    ];

    let mut test_lines = vec![];
    for line in raw_lines.iter() {
        let char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
        test_lines.push(char_vec);
    }
    let total = calculate(test_lines);
    println!("Total is {total}");
    assert!(total == 4361);
}

#[test]
fn test_number_at_end() {
    let raw_lines = vec![
        String::from("..*.....=1"),
        String::from("..1......."),
    ];

    let mut test_lines = vec![];
    for line in raw_lines.iter() {
        let mut char_vec: Vec<String> = line.chars().map(|c| c.to_string()).collect();
        char_vec.push(".".to_string());
        test_lines.push(char_vec);
    }
    let total = calculate(test_lines);
    println!("Total is {total}");
    assert!(total == 2);
}
