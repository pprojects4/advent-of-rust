use std::collections::HashMap;
use regex::Regex;

pub fn calculate(lines: Vec<String>) -> i32 {    
    let gear_regex = Regex::new(r"\*").unwrap();
    let num_regex = Regex::new(r"\d+").unwrap();
    let mut gears: HashMap<(i32, i32), Vec<i32>> = HashMap::new();
    for i in 0..lines.len() {
        let line = lines[i].clone();
        for gear_match in gear_regex.find_iter(line.as_str()) {
            gears.insert((i as i32, gear_match.start() as i32), vec![]);
            
        }
    }
    for i in 0..lines.len() {
        let line = lines[i].clone();
        for num_match in num_regex.find_iter(line.as_str()) {
            let start = num_match.start() as i32 - 1;
            let end = num_match.end() as i32 + 1;
            for index in start..end {
                if let Some(g) = gears.get_mut(&(i as i32, index as i32)) {
                    let num: i32 = num_match.as_str().parse().unwrap();
                    g.push(num);
                }
                if let Some(g) = gears.get_mut(&(i as i32 -1, index as i32)) {
                    let num: i32 = num_match.as_str().parse().unwrap();
                    g.push(num);
                }
                if let Some(g) = gears.get_mut(&(i as i32 +1, index as i32)) {
                    let num: i32 = num_match.as_str().parse().unwrap();
                    g.push(num);
                }
            }
        }
    }
    let mut result = 0;
    for entry in gears.values() {
        if entry.len() == 2 {
            result += entry[0] * entry[1];
        }
    }
    result
}


#[test]
fn test_sample_data() {
    let raw_lines = vec![
        String::from("467..114.."),
        String::from("...*......"),
        String::from("..35..633."),
        String::from("......#..."),
        String::from("617*......"),
        String::from(".....+.58."),
        String::from("..592....."),
        String::from("......755."),
        String::from("...$.*...."),
        String::from(".664.598.."),
    ];
    let total = calculate(raw_lines);
    println!("Total is {total}");
    assert!(total == 467835);
}