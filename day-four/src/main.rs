use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("input.txt").expect("Unable to open file");
    let reader = BufReader::new(file);

    let mut lines: Vec<String> = vec![];

    for raw_line in reader.lines() {
        match raw_line {
            Err(_) => panic!("Unable to parse line"),
            Ok(line) => lines.push(line.clone()),
        }
    }
    let result = calculate_part_one(lines);
    println!("Part One: {result}");
}

fn calculate_part_one(lines: Vec<String>) -> i32 {
    let mut result = 0;
    let num_regex = Regex::new(r"\d+").unwrap();
    for line in lines {
        let mut line_res = 0;
        let parts: Vec<&str> = line
            .split(":")
            .last()
            .unwrap()
            .split("|")
            .collect();
        let win_nums: Vec<&str> = num_regex.find_iter(parts[0]).map(|m| m.as_str()).collect();
        let cards: Vec<&str> = num_regex.find_iter(parts[1]).map(|m| m.as_str()).collect();
        for win_num in win_nums {
            if cards.contains(&win_num) {
                if line_res == 0 {
                    line_res = 1;
                } else {
                    line_res *= 2;
                }
            }
        }
        result += line_res;
    }
    result
}

#[test]
fn test_sample_input() {
    let input = vec![
        String::from("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"),
        String::from("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"),
        String::from("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"),
        String::from("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"),
        String::from("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"),
        String::from("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"),
    ];
    let result = calculate_part_one(input);
    println!("Result: {result}");
    assert!(result == 13)
}
